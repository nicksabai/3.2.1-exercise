/**
 * Server side code.
 */
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.NODE_PORT || 3000;

app.use(express.static(__dirname + "/../client/"));



var n = 5; //Define n

var A = []; // assign variable to empty array
var sum = 0; // set sum to 0

for (var i = 0; i <= n; i++) { // until condition is satisfied, add 1 until value > n
    A.push(i); // adds 1 item to the end of the array and returns new length
    
    sum += A[i]; // adds 1 to the sum until the above for loop function ends
}


console.log("The value of n is " + n);
console.log("The sum of the array is " + sum);

var elementNo = 5;
var index = A.indexOf(elementNo);
    if (elementNo > n, elementNo < 0) {
        console.log("Element not found");
    } else {
        console.log("The index of element " + elementNo + " is " + index);
    }
    

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});